<?php
/**
 * Created by PhpStorm.
 * User: maxime
 * Date: 02/02/17
 * Time: 07:50
 */

namespace BlogBundle\DataFixtures\ORM;

use BlogBundle\Entity\Article;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadArticleData extends AbstractFixture
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $firstArticle = $this->getFirstArticle();
        $manager->persist($firstArticle);

        $secondArticle = $this->getSecondArticle();
        $manager->persist($secondArticle);

        $manager->flush();
    }

    public function getFirstArticle()
    {
        $article    =   new Article();
        $article    ->  setEnable(true);
        $article    ->  setUser($this->getReference('user_account'));
        $article    ->  setDateAdd(new \DateTime('now'));
        $article    ->  setDateUpd(new \DateTime('now'));
        $article    ->  setTitle('Welcome to this blog!');
        $article    ->  setContent('This article is so great !');
        $article    ->  setPosts([]);

        $this->setReference('article_first', $article);


        return $article;
    }

    public function getSecondArticle()
    {
        $article    =   new Article();
        $article    ->  setEnable(true);
        $article    ->  setUser($this->getReference('user_account'));
        $article    ->  setDateAdd(new \DateTime('now'));
        $article    ->  setDateUpd(new \DateTime('now'));
        $article    ->  setTitle('This is the second article!');
        $article    ->  setContent('Sooooooo goood !');
        $article    ->  setPosts([]);

        $this->setReference('article_second', $article);


        return $article;
    }

    public function getOrder()
    {
        return 2;
    }

}