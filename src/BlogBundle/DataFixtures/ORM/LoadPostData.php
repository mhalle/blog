<?php
/**
 * Created by PhpStorm.
 * User: maxime
 * Date: 02/02/17
 * Time: 07:50
 */

namespace BlogBundle\DataFixtures\ORM;

use BlogBundle\Entity\Article;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadaArticleData extends AbstractFixture
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $firstArticle = $this->getFirstArticle();
        $manager->persist($firstArticle);

        $secondArticle = $this->getSecondArticle();
        $manager->persist($secondArticle);

        $manager->flush();
    }

    public function getFirstArticle()
    {
        $user = new Article();
        $user->setUsername('superadmin.maxime.halle');
        $user->setEmail('superadmin.test@test.fr');
        $user->setPlainPassword('superadmin_password');
        $user->setRoles(['ROLE_SUPER_ADMIN']);
        $user->setEnabled(true);

        $this->setReference('superadmin_user_account', $user);


        return $user;
    }

    public function getSecondArticle()
    {
        $user = new User();
        $user->setUsername('admin.maxime.halle');
        $user->setEmail('admin.test@test.fr');
        $user->setPlainPassword('admin_password');
        $user->setRoles(['ROLE_ADMIN']);
        $user->setEnabled(true);

        $this->setReference('admin_user_account', $user);

        return $user;
    }

    public function getOrder()
    {
        return 3;
    }

}