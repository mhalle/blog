window.twttr = (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0],
    t = window.twttr || {};
  if (d.getElementById(id)) return t;
  js = d.createElement(s);
  js.id = id;
  js.src = "https://platform.twitter.com/widgets.js";
  fjs.parentNode.insertBefore(js, fjs);

  t._e = [];
  t.ready = function(f) {
    t._e.push(f);
  };

  return t;
}(document, "script", "twitter-wjs"));


$(document).ready(function() {

    $(function(){
      parallaxInit('.parallax')
    });

    /*
    var  user_registration_url  = '/user/registration/add';
    var  redirection_url        = '/';

    //Events
    $('#registration').on('show.bs.modal', function (e) {
        var button = $(e.relatedTarget) // Button that triggered the modal
        var recipient = button.data('url');
        $('#event_remove_button').attr('href',recipient);
    });

    $('#registrationForm').submit(function(event) {
        var formData = {
            'name'                  : $('input#blogbundle_user_username').val(),
            'email'                 : $('input#blogbundle_user_email').val(),
            'password'              : $('input#blogbundle_user_password').val(),
            'password_confirmation'  : $('input#blogbundle_user_password_confirmation').val()
        };

        if(formData['password'] !== formData['password_confirmation'])
        {
            $('#registrationForm-flash').html('Password confirmation is not the same !');
            $('#registrationForm-flash').removeClass('hide');

        }else{
            $.ajax({
                type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                url         : user_registration_url, // the url where we want to POST
                data        : JSON.stringify(formData), // our data object
                dataType    : 'json', // what type of data do we expect back from the server
                encode      : true
            }).done(function(data) {console.log(data.status);
                if(data.status == 'success')
                {
                    $('#registrationForm-flash').removeClass('alert-danger').removeClass('hide');;
                    $('#registrationForm-flash').addClass('alert-success');
                    $('#registrationForm-flash').html(data.message);
                    $('#registrationForm-flash').show();
                    setTimeout(function(){
                        window.location.href = redirection_url;
                    }, 2000);

                }else{
                    $('#registrationForm-flash').removeClass('alert-success').removeClass('hide');
                    $('#registrationForm-flash').addClass('alert').addClass('alert-danger');
                    $('#registrationForm-flash').html(data.message);
                    $('#registrationForm-flash').show();
                }
            });
        }

        event.preventDefault();

    });*/
});