<?php

namespace BlogBundle\Form;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, [
                    'label' => 'form.title',
                ])
                ->add('content', CKEditorType::class, [
                    'label' => 'form.content',
                ])
                ->add('user', EntityType::class, [
                    'label' => 'form.user',
                    'class' => 'BlogUserBundle:User'
                ])
                ->add('enable', ChoiceType::class, [
                    'choices' => [
                        'Yes'   => 1,
                        'No'    => 0
                    ]
                ])
                ->add('image', FileType::class, [
                    'label'         => 'form.image',
                    'data_class'    => null,
                    'required'      => false
                ]);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $article    = $event->getData();
            $form       = $event->getForm();

            if ($article !== null && $article->getId() !== null) {
                $form->add('current_image', HiddenType::class, [
                    'label'     => 'form.image',
                    "mapped"    => false,
                    'data'      => $article->getImage()
                ]);
            }
        });
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BlogBundle\Entity\Article',
            'translation_domain' => 'article',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'blogbundle_article';
    }


}
