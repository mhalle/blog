<?php

namespace BlogBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PostControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');
    }

    public function testAddpost()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'post/add');
    }

    public function testEditpost()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'post/edit');
    }

    public function testDeletepost()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'post/delete');
    }

}
