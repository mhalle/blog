<?php
/**
 * Created by PhpStorm.
 * User: maxime
 * Date: 16/03/17
 * Time: 08:13
 */

namespace BlogBundle\Utils\FileUploader;

use Symfony\Component\HttpFoundation\File\UploadedFile;


class FileUploader
{
    private $destination;

    public function __construct($destination)
    {
        $this->destination = $destination;
    }

    public function upload(UploadedFile $file)
    {
        $fileName = md5(uniqid()).'.'.$file->guessExtension();

        $file->move($this->destination, $fileName);

        return $fileName;
    }

    public function getTargetDir()
    {
        return $this->destination;
    }
}