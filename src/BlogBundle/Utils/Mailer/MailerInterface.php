<?php
/**
 * Created by PhpStorm.
 * User: maxime
 * Date: 31/01/17
 * Time: 07:17
 */

namespace BlogBundle\Utils\Mailer;


interface MailerInterface
{
    public function getMessage($subject, $from, $to, $content);

    public function send();
}