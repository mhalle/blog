<?php
/**
 * Created by PhpStorm.
 * User: maxime
 * Date: 31/01/17
 * Time: 07:20
 */

namespace BlogBundle\Utils\Mailer;


class SwiftMailer implements MailerInterface
{
    protected $swiftMailer;
    protected $message;

    /**
     * SwiftMailer constructor. Get swift mailer
     *
     * @param \Swift_Mailer $swiftMailer
     */
    public function __construct(\Swift_Mailer $swiftMailer)
    {
        $this->swiftMailer = $swiftMailer;
    }

    /**
     * Make a message with mails parameters
     *
     * @param $subject
     * @param $from
     * @param $to
     * @param $content
     *
     * @return bool
     */
    public function getMessage($subject, $from, $to, $content)
    {
        $this->message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($from)
            ->setTo($to)
            ->setBody(
                $content,
                'text/html'
            );

        return true;
    }

    /**
     * Send Email
     *
     * @return boolean
     */
    public function send()
    {
        return (boolean)$this->swiftMailer->send($this->message);
    }
}