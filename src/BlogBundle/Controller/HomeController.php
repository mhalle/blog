<?php

namespace BlogBundle\Controller;

use BlogBundle\Entity\Article;
use BlogUserBundle\Form\UserRegistrationType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class HomeController extends Controller
{
    /**
     * @Route("/")
     * Blog index page
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $articles =   $em->getRepository('BlogBundle:Article')->findLastArticle(3);

        return $this->render('BlogBundle:Home:index.html.twig', [
            'articles'          => $articles
        ]);
    }

    /**
     * @Route("/admin/")
     * Admin Index Page
     */
    public function adminIndexAction()
    {
        return $this->render('BlogBundle:Admin:dashboard.html.twig');
    }
}
