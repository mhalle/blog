<?php

namespace BlogBundle\Controller;

use BlogBundle\Entity\Post;
use BlogBundle\Form\PostType;
use BlogBundle\Form\PostTypeWithArticle;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class PostController extends Controller
{
    /**
     * @Route("admin/post/", name="blogpost_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $posts = $em->getRepository('BlogBundle:Post')->findAll();

        return $this->render('BlogBundle:Post:index.html.twig', array(
            'posts' => $posts
        ));
    }

    /**
     * @Route("admin/post/add", name="blogpost_add")
     * @Method({"GET", "POST"})
     */
    public function addPostAction(Request $request)
    {
        $post = new Post();
        $currentUser = $this->get('security.token_storage')->getToken()->getUser();

        $form = $this->createForm(PostTypeWithArticle::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $postCreate   =   $form->getData();
            $postCreate->setUser($currentUser);
            $postCreate->setDateAdd(new \DateTime('now'));

            $em = $this->getDoctrine()->getManager();
            $em->persist($postCreate);
            $em->flush($postCreate);

            return $this->redirectToRoute('blogpost_index', array('id' => $post->getId()));
        }



        return $this->render('BlogBundle:Post:add.html.twig', array(
            'post' => $post,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/admin/post/edit/{id}", name="blogpost_edit")
     * @Method({"GET", "POST"})
     */
    public function editPostAction(Request $request, Post $post)
    {
        $editForm = $this->createForm('BlogBundle\Form\PostType', $post);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash(
                'success',
                $this->get('translator')->trans('form.edit.success', [], 'post')
            );

            return $this->redirectToRoute('blogpost_edit', array('id' => $post->getId()));
        }

        return $this->render('BlogBundle:Post:edit.html.twig', array(
            'post' => $post,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * @Route("/admin/post/delete/{id}", name="blogpost_delete")
     * @Method("GET")
     */
    public function deletePostAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $postRepository = $this->getDoctrine()->getRepository('BlogBundle:Post');

        $post =  $postRepository->findOneBy(['id' => $id]);

        if(!$post){
            $this->createNotFoundException('Ce post n\'existe plus');
        }

        $em->remove($post);
        $em->flush();

        $this->addFlash(
            'success',
            $this->get('translator')->trans('post.delete.success', [], 'post')
        );

        return $this->redirectToRoute('blogpost_index');
    }

}
