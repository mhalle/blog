<?php

namespace BlogBundle\Controller;

use BlogBundle\BlogBundle;
use BlogBundle\Entity\Article;
use BlogBundle\Entity\Post;
use BlogBundle\Form\PostType;
use BlogBundle\Form\PostWithoutUserType;
use Faker\Provider\ar_JO\Text;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Article controller.
 *
 */
class ArticleController extends Controller
{
    /**
     * Lists all article entities.
     *
     * @Route("/admin/article", name="blogarticle_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $articles = $em->getRepository('BlogBundle:Article')->findAll();

        return $this->render('BlogBundle:Article:index.html.twig', array(
            'articles' => $articles,
        ));
    }

    /**
     * Creates a new article entity.
     *
     * @Route("/admin/new", name="blogarticle_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $article = new Article();
        $currentUser = $this->get('security.token_storage')->getToken()->getUser();

        $form = $this->createFormBuilder($article)
                ->add('title')
                ->add('content', CKEditorType::class, [])
                ->add('image', FileType::class, [
                    'label' => 'form.image',
                ])
                ->add('user', EntityType::class, [
                    'class' => 'BlogUserBundle:User',
                    'data' => $currentUser
                ])->add('enable', ChoiceType::class, [
                    'choices' => [
                        'Yes'   => 1,
                        'No'    => 0
                    ]
                ])->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var Article
             */
            $articleCreate   =   $form->getData();
            $articleCreate->setDateUpd(new \DateTime('now'));
            $articleCreate->setDateAdd(new \DateTime('now'));

            $file = $articleCreate->getImage();

            $fileName = $this->get('file_uploader')->upload($file);

            $articleCreate->setImage($fileName);

            $em = $this->getDoctrine()->getManager();
            $em->persist($articleCreate);
            $em->flush($articleCreate);

            return $this->redirectToRoute('blogarticle_index', array('id' => $article->getId()));
        }

        return $this->render('@Blog/Article/add.html.twig', array(
            'article' => $article,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a article entity.
     *
     * @Route("/article/show/{id}", name="blogarticle_show")
     * @Method({"GET", "POST"})
     */
    public function showAction(Request $request, Article $article)
    {
        $user = $this->getUser();

        /**
         * @var Post
         */
        $post   =   new Post();
        
        if ($user === null) {
            $form = $this->createForm(PostWithoutUserType::class, $post, [
                'action' => $this->generateUrl('blogarticle_show', ['id' => $article->getId()]),
            ]);
        }else{
            $form = $this->createFormBuilder($post)
            ->add('comment', TextareaType::class, [
                'attr' => [
                    'rows' => 8
                ]
            ])
            ->getForm();
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $postCreate = $form->getData();
            $postCreate->setArticle($article);

            if ($user !== null) {
                $postCreate->setUser($user);
            }

            $postCreate->setDateAdd(new \DateTime('now'));

            $em = $this->getDoctrine()->getManager();
            $em->persist($postCreate);
            $em->flush($postCreate);

            return $this->redirectToRoute('blogarticle_show', array('id' => $article->getId()));
        }

        return $this->render('BlogBundle:Article:show.html.twig', array(
            'article'   =>  $article,
            'create_post'      =>  $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing article entity.
     *
     * @Route("/admin/article/edit/{id}", name="blogarticle_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Article $article)
    {

        $deleteForm = $this->createDeleteForm($article);
        $editForm = $this->createForm('BlogBundle\Form\ArticleType', $article);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            if($article->getImage() !== null)
            {
                $file = $article->getImage();

                $fileName = $this->get('file_uploader')->upload($file);

                $article->setImage($fileName);
            }else{
                $article->setImage($editForm->get('current_image')->getData());
            }

            $this->getDoctrine()->getManager()->flush();

            $this->addFlash(
                'success',
                $this->get('translator')->trans('form.edit.success', [], 'article')
            );

            return $this->redirectToRoute('blogarticle_edit', array('id' => $article->getId()));
        }

        return $this->render('BlogBundle:Article:edit.html.twig', array(
            'article' => $article,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a article entity.
     *
     * @Route("/admin/article/delete/{id}", name="blogarticle_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $articleRepository = $this->getDoctrine()->getRepository('BlogBundle:Article');

        $article =  $articleRepository->findOneBy(['id' => $id]);

        if(is_null($article)){
            throw $this->createNotFoundException('Cet article n\'existe plus');
        }

        $em->remove($article);
        $em->flush();

        $this->addFlash(
            'success',
            $this->get('translator')->trans('article.delete.success', [], 'article')
        );

        return $this->redirectToRoute('blogarticle_index');
    }

    /**
     * Creates a form to delete a article entity.
     *
     * @param Article $article The article entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Article $article)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('blogarticle_delete', array('id' => $article->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
