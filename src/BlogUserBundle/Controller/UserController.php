<?php
/**
 * Created by PhpStorm.
 * User: mhalle
 * Date: 23/01/17
 * Time: 21:37
 */

namespace BlogUserBundle\Controller;

use BlogUserBundle\BlogUserBundle;
use BlogUserBundle\Entity\User;
use BlogUserBundle\Form\UserProfileType;
use BlogUserBundle\Form\UserRegistrationType;
use BlogUserBundle\Form\UserType;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Util\PasswordUpdater;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;

class UserController extends Controller
{
    /**
     * Get list of users
     *
     * @Route("/admin/user", name="bloguser_list_index")
     */
    public function indexAction()
    {
        $userRepository = $this->getDoctrine()->getRepository('BlogUserBundle:User');
        $users = $userRepository->findAll();

        return $this->render('BlogUserBundle:User:index.html.twig', ['users' => $users]);
    }

    /**
     * Edit a user
     *
     * @Route("/admin/user/edit/{userId}", name="bloguser_edit")
     * @param Request $request
     * @param         $userId
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editUserAction(Request $request, $userId)
    {
        $userRepository = $this->getDoctrine()->getRepository('BlogUserBundle:User');
        $user = $userRepository->findOneBy(['id' => $userId]);

        if (!$user) {
            $this->createNotFoundException('Cet utilisateur n\'existe plus');
        }

        $form   =   $this->createForm(UserType::class, $user, array(
            'action' => $this->generateUrl('bloguser_edit', ['userId' => $user->getId()]),
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userUpdate   =   $form->getData();

            $entityManager  =   $this->getDoctrine()->getManager();
            $entityManager  ->  persist($userUpdate);
            $entityManager  ->  flush();

            $this->addFlash(
                'success',
                $this->get('translator')->trans('form.edit.success', [], 'user')
            );

        }

        return $this->render('BlogUserBundle:User:edit.html.twig', ['form' => $form->createView()]);
    }

    /**
     * Add a user
     *
     * @Route("/admin/user/add", name="bloguser_add")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addUserAction(Request $request)
    {
        $user = new User();

        $encoder = $this->container->get('security.password_encoder');
        $userRepository = $this->getDoctrine()->getRepository('BlogUserBundle:User');

        $form   =   $this->createForm(UserType::class, $user, array(
            'action' => $this->generateUrl('bloguser_add'),
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userCreate   =   $form->getData();
            $encoded = $encoder->encodePassword($user, $userCreate->getPassword());
            $userCreate->setPassword($encoded);
            $userCreate->setEnabled(true);


            if(!is_null($userRepository->alreadyExist($userCreate->getEmail())))
            {
                $this->addFlash(
                    'danger',
                    $this->get('translator')->trans('form.add.error', [], 'user')
                );
                return $this->render('BlogUserBundle:User:add.html.twig', ['form' => $form->createView()]);

            }

            $entityManager  =   $this->getDoctrine()->getManager();
            $entityManager  ->  persist($userCreate);
            $entityManager  ->  flush();

            $this->addFlash(
                'success',
                $this->get('translator')->trans('form.add.success', [], 'user')
            );

            $event = new UserEvent($userCreate);

            $eventDispatcher = $this->get('event_dispatcher');

            $eventDispatcher->dispatch(FOSUserEvents::USER_CREATED,$event);

            return $this->redirectToRoute('bloguser_list_index');

        }

        return $this->render('BlogUserBundle:User:add.html.twig', ['form' => $form->createView()]);
    }

    /**
     * Delete a user
     *
     * @Route("/delete/{userId}", name="bloguser_delete")
     */
    public function deleteAction($userId)
    {
        $em = $this->getDoctrine()->getManager();
        $userRepository = $this->getDoctrine()->getRepository('BlogUserBundle:User');

        $user =  $userRepository->findOneBy(['id' => $userId]);

        if(!$user){
            $this->createNotFoundException('Cet utilisateur n\'existe plus');
        }

        $em->remove($user);
        $em->flush();

        $this->addFlash(
            'success',
            $this->get('translator')->trans('user.delete.success', [], 'user')
        );

        return $this->redirectToRoute('bloguser_list_index');
    }

    /**
     * Edit User Profile
     *
     * @Route("/user/profile/edit", name="bloguser_profile_edit")
     */
    public function editProfileAction(Request $request)
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();
        $userRepository = $this->getDoctrine()->getManager()->getRepository('BlogUserBundle:User');
        $userRetrive    = $userRepository->findOneBy(['id' => $user->getId()]);
        $userPassword   = $userRetrive->getPassword();

        if (!$user) {
            $this->createNotFoundException('Cet utilisateur n\'existe plus');
        }

        $form   =   $this->createForm(UserProfileType::class, $user, array(
            'action' => $this->generateUrl('bloguser_profile_edit'),
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var User $userUpdate
             */
            $userUpdate   =   $form->getData();

            $password                   = $form->get('password')->getData();
            $newPassword                = $form->get('new_password')->getData();
            $newPasswordConfirmation    = $form->get('new_password_confirmation')->getData();

            if($userPassword != $password)
            {
                $this->addFlash(
                    'danger',
                    $this->get('translator')->trans('form.profile.password', [], 'user')
                );

                return $this->render('BlogUserBundle:User:edit.html.twig', ['form' => $form->createView()]);
            }else{
                $userUpdate->setPlainPassword($password);
            }

            if(!is_null($newPasswordConfirmation) && !is_null($newPassword) && $newPasswordConfirmation != $newPassword)
            {
                $this->addFlash(
                    'danger',
                    $this->get('translator')->trans('form.profile.password_confirmation', [], 'user')
                );

                return $this->render('BlogUserBundle:User:edit.html.twig', ['form' => $form->createView()]);
            }elseif(!is_null($newPasswordConfirmation) && !is_null($newPassword) && $newPasswordConfirmation === $newPassword){
                $userUpdate = $userUpdate->setPlainPassword($newPassword);
            }

            $entityManager  =   $this->getDoctrine()->getManager();
            $entityManager  ->  persist($userUpdate);
            $entityManager  ->  flush();

            $this->addFlash(
                'success',
                $this->get('translator')->trans('form.edit.success', [], 'user')
            );

        }

        return $this->render('BlogUserBundle:User:edit.html.twig', ['form' => $form->createView()]);
    }

    /**
     *
     * @Route("/user/registration/add", name="bloguser_registration_add")
     */
    public function registrationAction(Request $request){

        $jsonRequest    =   $request->getContent();
        $formData       =   json_decode($jsonRequest, true);

        if($formData['password'] == $formData['password_confirmation'])
        {
            $em                     = $this->getDoctrine()->getManager();
            $email                  = $formData['email'];
            $password               = $formData['password'];
            $username               = $formData['name'];

            $user                   = new User();

            $user->setEmail($email);
            $user->setPlainPassword($password);
            $user->setUsername($username);
            $em->persist($user);
            $em->flush();

            $response = new JsonResponse([
               'status'     => 'success',
               'message'    => 'You account has been created !'
            ], 200);

            $token = new UsernamePasswordToken($user, $user->getPassword(), 'main', $user->getRoles());

            $context = $this->get('security.token_storage');
            $context->setToken($token);
        }else{
            $response = new JsonResponse([
               'status'     => 'error',
               'message'    => 'Passwords must be the same !'
            ], 200);
        }

        return $response;
    }

    /**
     * @Route("/user/registration/form", name="bloguser_registration_form")
     */
    public function registrationFormAction()
    {
        $em = $this->getDoctrine()->getManager();

        $registrationForm = $this->createForm(UserRegistrationType::class);

        return $this->render('BlogBundle:Blog:registration.html.twig', [
            'registrationForm'  => $registrationForm->createView()
        ]);
    }



}