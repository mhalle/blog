<?php
/**
 * Created by PhpStorm.
 * User: mhalle
 * Date: 22/01/17
 * Time: 11:47
 */

namespace BlogUserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class BlogUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}