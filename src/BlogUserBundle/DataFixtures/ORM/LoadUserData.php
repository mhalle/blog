<?php
/**
 * Created by PhpStorm.
 * User: maxime
 * Date: 02/02/17
 * Time: 07:50
 */

namespace BlogUserBundle\DataFixtures\ORM;


use BlogUserBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserData extends AbstractFixture
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $userSuperAdmin = $this->getSuperAdmin();
        $manager->persist($userSuperAdmin);

        $userAdmin = $this->getAdmin();
        $manager->persist($userAdmin);

        $user = $this->getUser();
        $manager->persist($user);

        $manager->flush();
    }

    public function getSuperAdmin()
    {
        $user = new User();
        $user->setUsername('superadmin.maxime.halle');
        $user->setEmail('superadmin.test@test.fr');
        $user->setPlainPassword('superadmin_password');
        $user->setRoles(['ROLE_SUPER_ADMIN']);
        $user->setEnabled(true);

        $this->setReference('superadmin_user_account', $user);


        return $user;
    }

    public function getAdmin()
    {
        $user = new User();
        $user->setUsername('admin.maxime.halle');
        $user->setEmail('admin.test@test.fr');
        $user->setPlainPassword('admin_password');
        $user->setRoles(['ROLE_ADMIN']);
        $user->setEnabled(true);

        $this->setReference('admin_user_account', $user);

        return $user;
    }

    public function getUser()
    {
        $user = new User();
        $user->setUsername('maxime.halle');
        $user->setEmail('test@test.fr');
        $user->setPlainPassword('password');
        $user->setRoles(['ROLE_USER']);
        $user->setEnabled(true);

        $this->setReference('user_account', $user);

        return $user;
    }

    public function getOrder()
    {
        return 1;
    }
}