<?php
/**
 * Created by PhpStorm.
 * User: mhalle
 * Date: 29/01/17
 * Time: 20:03
 */

namespace BlogUserBundle\EventListener;


use BlogBundle\Utils\Mailer\MailerInterface;
use BlogBundle\Utils\Mailer\MailerMessageInterface;
use FOS\UserBundle\Event\UserEvent;
use Psr\Log\LoggerInterface;

class UserCreationMailListener
{
    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    protected $mailerMessage;

    public function __construct(MailerInterface $mailer,  LoggerInterface $logger)
    {
        $this->mailer = $mailer;
        $this->logger = $logger;
    }

    /**
     * @param UserEvent $user
     */
    public function onFosuserUserCreated(UserEvent $userEvent)
    {
        $user = $userEvent->getUser();

        $content = 'Your account has been created by administrator. Here is your credentials :<br/>
            Login: '.$user->getEmail().'<br/>
            Mot de passe: '.$user->getPassword();

        $message = $this->mailer->getMessage('Blog', 'no-reply@blog.com', $user->getEmail(), $content);

        //Add a log for the creation
        $this->logger->info('Administrator : User creation ('.$user->getEmail().')');

        //Send a mail to the user
        $this->mailer->send($message);
    }
}