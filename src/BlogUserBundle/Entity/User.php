<?php

namespace BlogUserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="BlogUserBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Article
     *
     * @ORM\OneToMany(targetEntity="BlogBundle\Entity\Article", mappedBy="user", cascade={"remove"})
     */
    protected $articles;

    /**
     * @var Post
     *
     * @ORM\OneToMany(targetEntity="BlogBundle\Entity\Post", mappedBy="user", cascade={"remove"})
     */

    private $posts;

    /**
     * Many Users have Many Users.
     * @ORM\ManyToMany(targetEntity="User", mappedBy="subscribers")
     */
    private $subscriptions;

    /**
     * Many Users have many Users.
     * @ORM\ManyToMany(targetEntity="User", inversedBy="subscriptions")
     * @ORM\JoinTable(name="user_subscription",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="subscriber_user_id", referencedColumnName="id")}
     *      )
     */
    private $subscribers;

    public function __construct()
    {
        parent::__construct();
        $this->articles         =   new ArrayCollection();
        $this->subscribers      =   new ArrayCollection();
        $this->subscriptions    =   new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User[]
     */
    public function getSubscribers()
    {
        return $this->subscribers;
    }

    /**
     * @param User[] $subscribers
     */
    public function setSubscribers($subscribers)
    {
        $this->subscribers = $subscribers;
    }

    /**
     * @return User[]
     */
    public function getSubscriptions()
    {
        return $this->subscriptions;
    }

    /**
     * @param User[] $Subscriptions
     */
    public function setSubscriptions($subscriptions)
    {
        $this->subscriptions = $subscriptions;
    }


}

