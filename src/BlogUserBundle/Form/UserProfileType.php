<?php

namespace BlogUserBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserProfileType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('username', TextType::class, [
            'label' => 'form.username',
            'attr'  => [
                'placeholder' => 'form.username',
            ],
        ])
            ->add('email', EmailType::class, [
                'label' => 'form.email',
                'attr'  => [
                    'placeholder' => 'form.email',
                ],
            ])
            ->add('password', PasswordType::class, [
                'always_empty' => true,
            ])
            ->add('new_password', PasswordType::class, [
                'always_empty'  =>  true,
                'mapped'        =>  false,
                'required'      =>  false
            ])
            ->add('new_password_confirmation', PasswordType::class, [
                'always_empty'  =>  true,
                'mapped'        =>  false,
                'required'      =>  false
            ])
            ->add('roles', ChoiceType::class, array(
                'label'       => 'form.roles.title',
                'choices'     => array(
                    'form.roles.super_admin' => 'ROLE_SUPER_ADMIN',
                    'form.roles.admin'       => 'ROLE_ADMIN',
                    'form.roles.user'        => 'ROLE_USER',
                ),
                'multiple'    => true,
                'mapped'      => true,
                'required'    => true,
                'placeholder' => 'form.role',
                'empty_data'  => null,
            ));

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'BlogUserBundle\Entity\User',
            'translation_domain' => 'user',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'blogbundle_user';
    }


}
