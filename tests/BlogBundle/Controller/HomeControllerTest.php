<?php

namespace Tests\BlogBundle\Controller;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class HomeControllerTest extends WebTestCase
{
    protected $client;

    public function getClientWithAuthenticatedUser()
    {
        $fixtures = $this->loadFixtures([
            'BlogUserBundle\DataFixtures\ORM\LoadUserData',
            'BlogBundle\DataFixtures\ORM\LoadArticleData'
        ])->getReferenceRepository();
        $this->loginAs($fixtures->getReference('superadmin_user_account'), 'main');

        $this->client = $this->makeClient();
    }

    public function testHomePage()
    {
        $this->getClientWithAuthenticatedUser();

        $crawler = $this->client->request('GET', '/');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertEquals(2, $crawler->filter('article')->count());
    }

    public function testAdminHomePage()
    {
        $this->getClientWithAuthenticatedUser();

        $crawler = $this->client->request('GET', '/admin/');

        $this->assertContains('Dashboard', $crawler->filter('h1')->text());

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

    public function testAdminUserPage()
    {
        $this->getClientWithAuthenticatedUser();

        $crawler = $this->client->request('GET', '/admin/user');

        $this->assertStatusCode(200, $this->client);

        $this->assertContains('Utilisateurs', $crawler->filter('h2')->text());

        $this->assertEquals($crawler->filter('tbody > tr')->count(), 3);

    }

    public function testAdminAddUserPage()
    {
        $this->getClientWithAuthenticatedUser();

        $crawler = $this->client->request('GET', '/admin/user');

        $this->assertStatusCode(200, $this->client);

        $this->assertContains('Utilisateurs', $crawler->filter('h2')->text());

        $this->assertEquals($crawler->filter('tbody > tr')->count(), 3);

        $this->tearDown();

    }


    /*
     * Login is still not use on the site
     *
    public function testNavBar()
    {
        $client = $this->makeClient(false);

        $crawler = $client->request('GET', '/');

        $this->assertContains('Inscription', $crawler->filter('#nav-signup')->text());
        $this->assertContains('Login', $crawler->filter('#nav-login')->text());
    }*/


}
