<?php

namespace BlogBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ArticleControllerTest extends \Liip\FunctionalTestBundle\Test\WebTestCase
{
    protected $client;

    public function getClientWithAuthenticatedUser()
    {
        $fixtures = $this->loadFixtures([
            'BlogUserBundle\DataFixtures\ORM\LoadUserData',
            'BlogBundle\DataFixtures\ORM\LoadArticleData'
        ])->getReferenceRepository();
        $this->loginAs($fixtures->getReference('superadmin_user_account'), 'main');

        $this->client = $this->makeClient();
    }

    public function testHomeArticleAdmin()
    {
        $this->getClientWithAuthenticatedUser();

        $crawler = $this->client->request('GET', '/admin/article');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $title = $crawler   ->filter('table.table-striped > tbody > tr')
                            ->first()
                            ->filter('td:nth-child(2)')->text();

        $this->assertEquals('Welcome to this blog!', $title);
    }

    public function testAdminAddArticlePage()
    {
        $this->getClientWithAuthenticatedUser();

        $crawler = $this->client->request('GET', '/admin/new');

        $this->assertStatusCode(200, $this->client);

        $this->assertContains('form.add.title', $crawler->filter('h2')->text());

        $this->tearDown();
    }

    public function testAdminEditArticlePage()
    {
        $this->getClientWithAuthenticatedUser();

        $crawler = $this->client->request('GET', '/admin/article');

        $editLink = $crawler   ->filter('table.table-striped > tbody > tr')
                            ->first()
                            ->filter('td:nth-child(6) a')->first()->attr('href');

        $crawler = $this->client->request('GET', $editLink);

        $this->assertStatusCode(200, $this->client);

        $this->assertContains('Modification d\'un article', $crawler->filter('h2')->text());

        $this->tearDown();
    }

    public function testAdminDeleteArticlePage()
    {
        $this->getClientWithAuthenticatedUser();

        $this->client->followRedirects();

        $crawler = $this->client->request('GET', '/admin/article');

        $deleteLink = $crawler   ->filter('table.table-striped > tbody > tr')
                            ->first()
                            ->filter('td:nth-child(6) a:nth-child(2)')->attr('href');

        $crawler = $this->client->request('GET', $deleteLink);

        $this->assertStatusCode(200, $this->client);

        $this->assertContains('L\'Article a bien été supprimé', $crawler->filter('div.alert-success')->text());

        $this->client->request('GET', '/admin/article/delete/100');
        $this->assertStatusCode(404, $this->client);

        $this->tearDown();
    }

}
