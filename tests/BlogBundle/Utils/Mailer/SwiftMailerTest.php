<?php
/**
 * Created by PhpStorm.
 * User: maxime
 * Date: 01/02/17
 * Time: 08:30
 */

namespace Tests\BlogBundle\Utils\Mailer;


use BlogBundle\Utils\Mailer\SwiftMailer;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class SwiftMailerTest extends KernelTestCase
{
    private $container;

    private $fakeMail = [
        'subject'   =>  'Fake Mail',
        'from'      =>  'test@blog.fr',
        'to'        =>  'maxforiphone@gmail.com',
        'content'   =>  'Welcome to test area ! '
    ];

    public function setUp()
    {
        self::bootKernel();

        $this->container = self::$kernel->getContainer();
    }


    public function testGetMessage()
    {
        $mailer = new SwiftMailer($this->container->get('mailer'));
        $this->assertTrue($mailer->getMessage($this->fakeMail['subject'], $this->fakeMail['from'], $this->fakeMail['to'], $this->fakeMail['subject']));
    }

    public function testSendMail()
    {
        $mailer = new SwiftMailer($this->container->get('mailer'));

        $mailer->getMessage($this->fakeMail['subject'], $this->fakeMail['from'], $this->fakeMail['to'], $this->fakeMail['subject']);

        $this->assertEquals(1,$mailer->send());

    }
}