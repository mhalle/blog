<?php
/**
 * Created by PhpStorm.
 * User: maxime
 * Date: 01/02/17
 * Time: 08:30
 */

namespace Tests\BlogBundle\Utils\FileUploader;

use BlogBundle\Utils\FileUploader\FileUploader;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploaderTest extends KernelTestCase
{
    private $container;

    private $destination = 'images/';

    public function setUp()
    {
        self::bootKernel();

        $this->container = self::$kernel->getContainer();
    }


    public function testGetDestination()
    {
        $fileuploader = new FileUploader($this->destination);
        $this->assertEquals($this->destination, $fileuploader->getTargetDir());
    }

    public function testUpload()
    {
        $file           =   $this->getMockBuilder(UploadedFile::class)
                                ->disableOriginalConstructor()
                                ->getMock();

        $file           ->  method('guessExtension')
                        ->  willreturn('jpg');

        $file           ->  method('move')
                        ->  willReturn(true);

        $fileuploader   =   new FileUploader($this>$this->destination);

        $this->assertEquals('jpg', $file->guessExtension());
        $this->assertRegExp('/(.*)(.jpg)/',$fileuploader->upload($file));


    }
}