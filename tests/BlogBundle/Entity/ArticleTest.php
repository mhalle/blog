<?php
/**
 * Created by PhpStorm.
 * User: mhalle
 * Date: 04/08/17
 * Time: 13:21
 */

namespace BlogBundle\Tests\Entity;


use BlogBundle\Entity\Article;
use Entities\User;
use PHPUnit\Framework\TestCase;

class ArticleTest extends TestCase
{
    public function testArticleInitialisation(){
        $user       =   $this->getMock(User::class);

        $article    =   new Article();
        $article    ->  setTitle('Test Article');
        $article    ->  setContent('Welcome to the blog test !!! ');
        $article    ->  setDateAdd(new \DateTime());
        $article    ->  setDateUpd(new \DateTime());
        $article    ->  setPosts([]);
        $article    ->  setUser($user);
        $article    ->  setEnable(true);
        $article    ->  setImage('images/test');

        $this->assertInstanceOf(User::class, $article->getUser());
        $this->assertContains('Welcome', $article->getContent());
        $this->assertContains('Article', $article->getTitle());
        $this->assertTrue($article->getEnable());
        $this->assertInstanceOf(\DateTime::class, $article->getDateUpd());
        $this->assertInstanceOf(\DateTime::class, $article->getDateAdd());
        $this->assertEquals([], $article->getPosts());
        $this->assertEquals('images/test', $article->getImage());
    }
}