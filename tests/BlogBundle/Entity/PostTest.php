<?php
/**
 * Created by PhpStorm.
 * User: mhalle
 * Date: 04/08/17
 * Time: 13:21
 */

namespace BlogBundle\Tests\Entity;

use BlogBundle\Entity\Article;
use BlogBundle\Entity\Post;
use PHPUnit\Framework\TestCase;

class PostTest extends TestCase
{
    public function testPostInitialisation(){
        $article    =   $this->getMockBuilder(Article::class)->getMock();
        $article    ->  method('getContent')
                    ->  willReturn('Welcome Home');

        $user       =   $this->getMock(User::class);

        $post       =   new Post();
        $post       ->  setName('My Name');
        $post       ->  setComment('What a great article');
        $post       ->  setDateAdd(new \DateTime());
        $post       ->  setArticle($article);
        $post       ->  setUser($user);

        $this       ->  assertInstanceOf(User::class, $post->getUser());
        $this       ->  assertInstanceOf(Article::class, $post->getArticle());
        $this       ->  assertContains('Welcome', $article->getContent());
        $this       ->  assertContains('Name', $post->getName());
        $this       ->  assertContains('great', $post->getComment());
        $this       ->  assertInstanceOf(\DateTime::class, $post->getDateAdd());
    }
}