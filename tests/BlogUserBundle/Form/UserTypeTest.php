<?php
/**
 * Created by PhpStorm.
 * User: maxime
 * Date: 06/02/17
 * Time: 08:22
 */

namespace Tests\BlogUserBundle\Form;


use BlogUserBundle\Entity\User;
use BlogUserBundle\Form\UserType;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Tests\Form\Type\TypeTestCase;

class UserTypeTest extends TypeTestCase
{
    public function testSubmitValidData()
    {
        $formData = array(
            'username'  => 'test.test',
            'email'     => 'test@test.fr',
            'roles'     => ['ROLE_USER'],
            //'groups'    => new ArrayCollection,
        );

        $form = $this->factory->create(UserType::class);

        $user = new User();
        $user->setEmail($formData['email']);
        $user->setUsername('test.test');
        $user->setRoles(['ROLE_USER']);

        // submit the data to the form directly
        $form->submit($formData);

        $userForm = $form->getData();

        //launch getGroups in order to initialize groups field to ArrayCollection
        $user->getGroups();

        $this->assertTrue($form->isSynchronized());
        $this->assertEquals($user, $userForm);

        $view = $form->createView();
        $children = $view->children;

        foreach (array_keys($formData) as $key) {

            $this->assertArrayHasKey($key, $children);
        }
    }
}