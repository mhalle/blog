<?php
/**
 * Created by PhpStorm.
 * User: maxime
 * Date: 06/02/17
 * Time: 10:33
 */

namespace Tests\BlogUserBundle\Entity;


use BlogUserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class UserTest extends TestCase
{
    public function testUserEntity()
    {
        $userTest =  new User();
        $userTest->setEmail('test@test.fr');
        $userTest->setUsername('test');
        $userTest->setPassword('test');
        $userTest->setRoles(['ROLE_USER']);
        $userTest->setEnabled(true);
        
        $user = new User();
        $user->setEmail('testSubscriber@test.fr');
        $user->setUsername('testSubscriber');
        $user->setPassword('testSubscriber');
        $user->setRoles(['ROLE_USER']);
        $user->setEnabled(true);
        $user->setSubscribers($userTest);
        $user->setSubscriptions($userTest);

        $this->assertEquals($user->getSubscribers(), $userTest);
        $this->assertEquals($user->getSubscriptions(), $userTest);



    }
}